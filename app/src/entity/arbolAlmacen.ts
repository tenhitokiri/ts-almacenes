import { Entity, Tree, Column, PrimaryGeneratedColumn, TreeChildren, TreeParent } from 'typeorm';

@Entity()
export //@Tree("materialized-path")
class ArbolAlmacenes {
	@PrimaryGeneratedColumn() id: number;

	@Column({ length: 50 })
	nombre: string;

	@Column({ length: 10 })
	codigo: string;

	@Column({ length: 150 })
	descripcion: string;

	@Column('int', { default: 0 })
	parentid: number;

	@Column() permiso_cargo: boolean;

	@Column() permiso_descargo: boolean;

	@Column() es_logico: boolean;

	@Column() acepta_puesto: boolean;

	//@TreeChildren() children: ArbolAlmacenes[];

	//@TreeParent() parent: ArbolAlmacenes;
}
