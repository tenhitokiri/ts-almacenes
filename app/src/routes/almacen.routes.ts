import { Router } from 'express';

const router = Router();
import { getAlmacenes, createAlmacen } from '../controllers/arbolAlmacen.controller';

router.get('/arbol', getAlmacenes);
router.post('/arbol', createAlmacen);

export default router;