import { Any } from 'typeorm';
import Almacenes from './interface/Almacen';
import { ArbolAlmacenes } from './entity/arbolAlmacen';

export function compareValues(key: string, order = 'asc') {
	return function innerSort(a: any, b: any) {
		if (!a.hasOwnProperty(key) || !b.hasOwnProperty(key)) {
			// property doesn't exist on either object
			return 0;
		}

		const varA = typeof a[key] === 'string' ? a[key].toUpperCase() : a[key];
		const varB = typeof b[key] === 'string' ? b[key].toUpperCase() : b[key];

		let comparison = 0;
		if (varA > varB) {
			comparison = 1;
		} else if (varA < varB) {
			comparison = -1;
		}
		return order === 'desc' ? comparison * -1 : comparison;
	};
}

export const _makeTree = (arbol: ArbolAlmacenes[]): {} => {
	/* Función que recibe un arbol completo y ordenado por el campo parentId y luego separa los registros en 3 conjuntos:
	- Raices (nodos sin padres)
	- Ramas (nodos con padres e hijos)
	- Hojas (Nodos con padres sin hijos)
*/
	let resp: {};
	let hojas: any[];
	let found: boolean;
	let idRamas: number[]; //arreglo de id de ramas
	let idRaices: number[]; //arreglo de ID de Raices
	let idHojas: number[]; //arreglo de ID de Hojas
	let arbolTmp, ramasTmp, hojasTmp: Almacenes[];
	let hojatmp: Almacenes;
	let itemId: number;

	hojas = [];
	//*
	//Transformar AlmacenSql a Almacenes (incluye children {})
	arbol.forEach((rama) => {
		hojatmp = {
			id: rama.id,
			codigo: rama.codigo,
			nombre: rama.nombre,
			descripcion: rama.descripcion,
			acepta_puesto: rama.acepta_puesto,
			permiso_descargo: rama.permiso_descargo,
			es_logico: rama.es_logico,
			permiso_cargo: rama.permiso_cargo,
			parentid: rama.parentid,
			children: {}
		};
		hojas.push(hojatmp); //se guarda la hoja en una lista temporal
	});
	arbolTmp = hojas;

	// Ramas y hojas del arbol
	let ramas = arbolTmp.filter((item) => {
		return item.parentid !== 0;
	});
	//while (ramas.length > 1) {
	// Solo Hojas
	hojas = [];
	ramas.forEach((nodo) => {
		found = false;
		ramas.forEach((rama) => {
			if (rama.parentid === nodo.id) {
				found = true;
			}
			//console.log(found);
		});
		if (!found) {
			hojas.push(nodo);
		}
	});

	//Armar el arbol
	arbolTmp.forEach((rama) => {
		hojasTmp = [];
		hojas.forEach((hoja) => {
			//si la hoja actual tiene parentid igual al padre actual
			if (hoja.parentid == rama.id) {
				//armar la hoja con la nueva informacion
				hojasTmp.push(hoja);
				itemId = ramas.findIndex((v) => v.id == hoja.id);
				ramas.splice(itemId, itemId >= 0 ? 1 : 0);
				//				ramas.splice(ramas.findIndex((v) => v.id == hoja.id), 1);
				//console.table(ramas);
			}
		});
		rama.children = { data: hojasTmp };
	});
	//	}

	resp = {
		arbolTmp,
		ramas
	};
	return resp;
};
