import 'reflect-metadata';
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import userRoutes from './routes/user.routes';
import almacenRoutes from './routes/almacen.routes';
import { createConnection } from 'typeorm';

const app = express();
createConnection();

app.set('port', process.env.PORT || 3000);

//middlewares
app.use(cors());
app.use(morgan('dev'));
app.use(express.json());

//routes
app.use(userRoutes);
app.use(almacenRoutes);

app.get('/', (req, res) => {
	res.send('hola');
});

const main = async () => {
	await app.listen(app.get('port'), '0.0.0.0', () => {
		console.log(`Servidor ejecutandose en el puerto: ${app.get('port')}`);
	});
};

main();
