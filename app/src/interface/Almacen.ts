export default interface Almacenes {
	id: number;
	nombre: string;
	codigo: string;
	descripcion: string;
	parentid: number;
	permiso_cargo: boolean;
	permiso_descargo: boolean;
	es_logico: boolean;
	acepta_puesto: boolean;
	children: {};
};
