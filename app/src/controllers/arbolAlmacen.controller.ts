import { Request, Response, response } from 'express';
import { ArbolAlmacenes } from '../entity/arbolAlmacen';
import { compareValues, _makeTree } from '../functions';
import { getRepository, getTreeRepository, getManager } from 'typeorm';
import { resourceUsage } from 'process';
//const manager = getManager();

export const getAlmacenes = async (req: Request, res: Response): Promise<Response> => {
	const almacenes = await getRepository(ArbolAlmacenes).find();
	const ordenado = almacenes.sort(compareValues('parentid'));

	return res.json(_makeTree(ordenado));
};

export const createAlmacen = async (req: Request, res: Response): Promise<Response> => {
	const newAlmacen = getRepository(ArbolAlmacenes).create(req.body);
	console.table(newAlmacen);
	const results = await getRepository(ArbolAlmacenes).save(newAlmacen);
	return res.json(results);
};
